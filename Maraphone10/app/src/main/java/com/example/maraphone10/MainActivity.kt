package com.example.maraphone10

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import java.io.Serializable




class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun btnGoToInfoActivityonClick(view: View) {
        val human = Person()
        human.name = findViewById<EditText>(R.id.editTextName).text.toString()
        human.secondName = findViewById<EditText>(R.id.editTextSecondName).text.toString()
        human.fathersName = findViewById<EditText>(R.id.editTextFathersName).text.toString()
        human.age = findViewById<EditText>(R.id.editTextAge).text.toString()
        human.hobby = findViewById<EditText>(R.id.editTextHobby).text.toString()
        val intentToDo = Intent(this, InfoActivity::class.java)
        intentToDo.putExtra("HUMAN_DATA", human)
        startActivity(intentToDo)

        /*Intent(this, InfoActivity::class.java).also {
            startActivity(it)
        }*/

    }
}