package com.example.maraphone10

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.info_activity)


        val human = intent.getSerializableExtra("HUMAN_DATA") as? Person
        findViewById<TextView>(R.id.textViewName).text = getString(R.string.name_info, human?.name)
        findViewById<TextView>(R.id.textViewName3).text = getString(R.string.secondname_info, human?.secondName)
        findViewById<TextView>(R.id.textViewName4).text = getString(R.string.fathersname_info, human?.fathersName)
        findViewById<TextView>(R.id.textViewName5).text = getString(R.string.age_info, human?.age.toString())
        findViewById<TextView>(R.id.textViewName6).text = getString(R.string.hobby_info, human?.hobby)
        //Toast.makeText(this, Ivan?.name, Toast.LENGTH_SHORT).show()
    }
}